# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Forum
* Key functions (add/delete)
    1. 使用者頁面
    2. 貼文列表
    3. 貼文頁面
    4. 回覆任意貼文
* Other functions (add/delete)
    1. 每個帳號都有自己的頭像，若還沒有上傳會顯示預設頭像。
    2. 每篇貼文與回覆都會附帶發文時的時間，顯示於貼文上方。
    3. 貼文列表有分成五個，也就是五個討論版，可以針對不同主題進行討論。
    4. 可以在貼文列表或貼文頁面中點擊自己或他人頭像來檢視其個人頁面。
    5. 個人頁面可顯示的資料包含:名字、性別、生日、興趣與自我介紹。
    6. 資料還沒跑出來的時候會顯示載入中的動圖。

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
進入index.html後，左邊會載入一個navbar，可以在這個bar上登入、用google登入或是創建新帳號；<br />
登入後會載入頭像和名字，若仍未設定會顯示預設頭像和email；同時檢查有無設定過個人資料，若無則生成一筆預設的資料傳至database。<br />
右邊會顯示固定的五個討論版，使用者可不登入直接點擊進入討論版(postlist.html)瀏覽現存文章，<br />
但仍需透過討論版左手邊的navbar登入，才可看到其他使用者的頭像、進入貼文頁面(postpage.html)觀看別人對於這則貼文的回覆，進而回覆該篇貼文。<br />
登入後，在討論版上可以將欲發布的貼文輸入navbar中的New Post中點下submit發布貼文。<br />
在貼文列表和貼文頁面中皆可點擊他人或自己的頭像連到該使用者的使用者頁面(profile.html)，若檢視的是自己的頁面，則可點擊edit編輯個人資料、上傳頭貼。<br />
## Security Report (Optional)
將database中的規則改成若沒登入就不能寫入，storage則是沒登入就不能讀寫，防止非使用者亂動資料。