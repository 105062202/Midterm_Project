function init(){
    var user_email = '';
    var user_name = '';
    var side_menu = document.getElementById('side');
    var div_before_element="<div class='info-input'>";
    var button_back_home="<button class='btn btn-lg btn-primary btn-block' onclick='back_to_home()' style='background-color: '>Homepage</button>"
    var div_after_element="</div>";
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            user_email = user.email;
            user_name = user.name;
            var string_before_email="<img class='mb-4' src='img/icon.jpg' alt=''id='nav-pic'><strong>";
            var string_after_email="</strong><br><br><button class='btn btn-lg btn-primary btn-block' onclick='jump_to_profile()' id='btnprofile'>Profile</button><button class='btn btn-lg btn-primary btn-block' id='logout-btn'>Logout</button>";
            var string_post_submit="<div class='my-3 p-3 rounded box-shadow' style='text-align: center;'><h5 class='border-bottom border-gray pb-2 mb-0'>New Post</h5><textarea class='form-control' rows='5' id='comment' style='resize:none'></textarea><div class='media text-muted pt-3'><button id='post_btn' type='button' class='btn btn-lg btn-primary' style='margin:0 auto;'>Submit</button></div></div> ";
            side_menu.innerHTML=div_before_element+string_before_email+user_email+string_after_email+button_back_home+div_after_element+string_post_submit;
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().catch(function(error){alert(error.message)});
            });
            post_btn = document.getElementById('post_btn');
            post_txt = document.getElementById('comment');
            post_btn.addEventListener('click', function () {
                if (post_txt.value != "") {
                    /// TODO 6: Push the post to database's "com_list" node
                    ///         1. Get the reference of "com_list"
                    ///         2. Push user email and post data
                    ///         3. Clear text field
                    firebase.database().ref('com_list_01').push({
                        email: user_email,
                        data: post_txt.value
                    }).catch(e => console.log(e.message));
                    post_txt.value="";
                }
            });
            show_list(user);
        }
        else{
            var input_element="<img class='mb-4' src='img/pic2-01.png' alt=''id='nav-pic'><input type='email' id='inputEmail' class='form-control' placeholder='Email address' required autofocus ><input type='password' id='inputPassword' class='form-control' placeholder='Password' required><br>";
            var button_element="<button class='btn btn-lg btn-primary btn-block' id='btnLogin'>Login</button>";
            var button_face="<button id='facebook-sign-in' class='btn btn-lg btn-info btn-block'>FB Login</button>";
            var button_google="<button id='btngoogle' class='btn btn-lg btn-info btn-block'>Google Login</button>";
            var button_signup="<button class='btn btn-lg btn-secondary btn-block' id='btnSignUp'>New account</button>";
            side_menu.innerHTML=div_before_element+input_element+button_element+button_face+button_google+button_signup+button_back_home+div_after_element;
            login();
            show_list(user);
        }
    });
}
function show_list(user){
    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3' ><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div>";

    var postsRef = firebase.database().ref('com_list_01');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childSnapshot) {
                var childData = childSnapshot.val();
                console.log(childSnapshot.key);
                var first_after="<br><button class='btn btn-lg btn-primary' onclick='reply(&apos;"+childSnapshot.key+"&apos;)' id='btnreply'>Reply</button></div>\n";
                if(user){total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content + first_after;}
                else{total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content+"</div>\n";}
                first_count+=1;
            });
            document.getElementById('post_list').innerHTML=(total_post.join(''));
            postsRef.on('child_added',function(data){
                second_count+=1;
                if(second_count>first_count){
                    var childData = data.val();
                    var second_after="<br><button class='btn btn-lg btn-primary' onclick='reply(&apos;"+data.key+"&apos;)' id='btnreply'>Reply</button></div>\n";
                    //total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content + second_after;
                    if(user){$('#post_list').append(str_before_username + childData.email + "</strong>" + childData.data + str_after_content + second_after);}
                    else{$('#post_list').append(str_before_username + childData.email + "</strong>" + childData.data + str_after_content+"</div>\n");}
                }
            })
        })
        .catch(e => console.log(e.message));
       
    
}
function reply(key_in){
    var postsRef = firebase.database().ref('com_list_01');
    //postsRef
    console.log(key_in);
}
function login(){
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    btnLogin.addEventListener('click', e => {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).catch(e => alert(e.message));
        });
    btnGoogle.addEventListener('click', e => {
        var provider = new firebase.auth.GoogleAuthProvider();
        alert('signInWithPopup');
        firebase.auth().signInWithPopup(provider).catch(function (error) {
            alert('error: ' + error.message);
        })});
    btnSignUp.addEventListener('click', e => {        
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email, password).catch(e => alert(e.message));
        });
    }
function back_to_home(){
    window.location.href="index.html";
}
function jump_to_profile(){
    window.location.href="profile.html";
}
window.onload = function () {
    init();
};
