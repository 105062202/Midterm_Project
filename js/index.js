var storageRef = firebase.storage().ref();
function init(){
    var user_email = '';
    var user_name = '';
    var side_menu = document.getElementById('side');
    var div_before_element="<div class='info-input'>";
    var div_after_element="</div>";
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            var Ref = firebase.database().ref("userdata/"+user.uid);
            var username='';
            user_email = user.email;
            user_name = user.name;
            var string_before_email="<img class='mb-4' src='img/ssa.gif' alt=''id='nav-pic'><strong style='font-size: 1.5vmin;'>";
            var string_after_email="</strong><br><br><button class='btn btn-lg btn-primary btn-block' onclick='jump_to_profile()' id='btnprofile'>Profile</button><button class='btn btn-lg btn-primary btn-block' id='logout-btn'>Logout</button>";
            var Ref = firebase.database().ref("userdata/"+user.uid);
            Ref.once('value').then(function(snapshot) {
                var temp = snapshot.val();
                //alert(temp);
                if(!temp){
                    Ref.update({
                        name:user.email,
                        gender:"unknown",
                        birth: "2000-01-01",
                        hobby: "unknown",
                        introduce: "Hello"
                    })
                }
            });
            Ref.once('value').then(function(snapshot) {
                side_menu.innerHTML=div_before_element+string_before_email+"<span id='user_e'>"+snapshot.val().name+"</span>"+string_after_email+div_after_element;
                var btnLogout = document.getElementById('logout-btn');
                btnLogout.addEventListener('click', e => {
                    firebase.auth().signOut().catch(function(error){alert(error.message)});
                });
                var user_id=user.uid;
                    storageRef.child("/test/"+user_id).getDownloadURL().then(function (url) {
                        // `url` is the download URL for fileRefDL.value
                        document.getElementById('nav-pic').src=url;
                    }).catch(function (error) {
                        console.log(error.message);
                        storageRef.child("/test/default").getDownloadURL().then(function (url) {
                            document.getElementById('nav-pic').src=url;
                        });
                    });
            });
            
            
            
            
        }
        else{
            var input_element="<img class='mb-4' src='img/pic2-01.png' alt=''id='nav-pic'><input type='email' id='inputEmail' class='form-control' placeholder='Email address' required autofocus ><input type='password' id='inputPassword' class='form-control' placeholder='Password' required><br>";
            var button_element="<button class='btn btn-lg btn-primary btn-block' id='btnLogin'>Login</button>";
            var button_google="<button id='btngoogle' class='btn btn-lg btn-info btn-block'>Google Login</button>";
            var button_signup="<button class='btn btn-lg btn-secondary btn-block' id='btnSignUp'>New account</button>";
            side_menu.innerHTML=div_before_element+input_element+button_element+button_google+button_signup+div_after_element;
            login();
        }
    });
}

function jump_to_01(){
    window.location.href="postlist.html?list_01";
}
function jump_to_02(){
    window.location.href="postlist.html?list_02";
}
function jump_to_03(){
    window.location.href="postlist.html?list_03";
}
function jump_to_04(){
    window.location.href="postlist.html?list_04";
}
function jump_to_05(){
    window.location.href="postlist.html?list_05";
}
function jump_to_profile(){
    firebase.auth().onAuthStateChanged(function (user) {
        window.location.href="profile.html?"+user.uid;
    });
}

function login(){
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    btnLogin.addEventListener('click', e => {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).catch(e => alert(e.message));
        });
    btnGoogle.addEventListener('click', e => {
        var provider = new firebase.auth.GoogleAuthProvider();
        alert('signInWithPopup');
        firebase.auth().signInWithPopup(provider).catch(function (error) {
            alert('error: ' + error.message);
        })});
    btnSignUp.addEventListener('click', e => {        
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email, password).catch(e => alert(e.message));
        });
}/*
$('#side').on('click','.fuck',function(){
    alert('fuck');
});
$(document).ready(function() {
    alert('fuck');
});
$(document).ready(function () {
    $('div').on('click', function() {
       console.log("Clicked.");
    });
});*/
window.onload = function () {
    init();
};
