var user_email = '';
var query;
var storageRef = firebase.storage().ref();
function init(){
    query = window.location.search;
    // Skip the leading ?, which should always be there, 
    // but be careful anyway
    if (query.substring(0, 1) == '?') {
        query = query.substring(1);
    }
    if(query.length<1){
        back_to_home();
    }
    //alert("data_sent "+query);
    document.getElementById('list_title').innerHTML=query;
    var user_name = '';
    var side_menu = document.getElementById('side');
    var div_before_element="<div class='info-input'>";
    var button_back_home="<button class='btn btn-lg btn-primary btn-block' onclick='back_to_home()' style='background-color: '>Home</button>"
    var div_after_element="</div>";
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            var Ref = firebase.database().ref("userdata/"+user.uid);
            Ref.once('value').then(function(snapshot) {
                var temp = snapshot.val();
                //alert(temp);
                if(!temp){
                    Ref.update({
                        name:user.email,
                        gender:"unknown",
                        birth: "2000-01-01",
                        hobby: "unknown",
                        introduce: "Hello"
                    })
                }
            });
            user_email = user.email;
            user_name = user.name;
            var string_before_email="<img class='mb-4' src='img/ssa.gif' alt=''id='nav-pic'><span style='font-size: 1.5vmin;'>";
            var string_after_email="</span><br><br><button class='btn btn-lg btn-primary btn-block' onclick='jump_to_profile()' id='btnprofile'>Profile</button><button class='btn btn-lg btn-primary btn-block' id='logout-btn'>Logout</button>";
            var string_post_submit="<div class='my-3 p-3 rounded box-shadow' style='text-align: center;'><p class='border-bottom border-gray pb-2 mb-0' style='font-size: 3vmin;'>New Post</p><textarea class='form-control' rows='5' id='comment' style='resize:none'></textarea><div class='media text-muted pt-3'><button id='post_btn' type='button' class='btn btn-lg btn-primary' style='margin:0 auto;padding:0!important;'>Submit</button></div></div> ";
            Ref.once('value').then(function(snapshot) {
                side_menu.innerHTML=div_before_element+string_before_email+snapshot.val().name+string_after_email+button_back_home+div_after_element+string_post_submit;
                var btnLogout = document.getElementById('logout-btn');
                btnLogout.addEventListener('click', e => {
                    firebase.auth().signOut().then(function(){window.location.href="postlist.html?"+query;}).catch(function(error){alert(error.message)});
                });
                var user_id=user.uid;
                    storageRef.child("/test/"+user_id).getDownloadURL().then(function (url) {
                        // `url` is the download URL for fileRefDL.value
                        document.getElementById('nav-pic').src=url;
                    }).catch(function (error) {
                        console.log(error.message);
                        storageRef.child("/test/default").getDownloadURL().then(function (url) {
                            document.getElementById('nav-pic').src=url;
                        });
                    });
                post_btn = document.getElementById('post_btn');
                post_txt = document.getElementById('comment');
                
                //alert(date_posted);
                post_btn.addEventListener('click', function () {
                    if (post_txt.value != "") {
                        var time =new Date();
                        var hour = (time.getUTCHours()+8)%24;
                        var te_hour= hour>10?hour:"0"+hour;
                        var min = time.getUTCMinutes()>9?time.getUTCMinutes():"0"+time.getUTCMinutes();
                        var sec = time.getUTCSeconds()>9?time.getUTCSeconds():"0"+time.getUTCSeconds();
                        var date_posted= time.getUTCFullYear()+"/"+(time.getUTCMonth()+1)+"/"+(time.getUTCDate()+1)+" "+te_hour+":"+min+":"+sec;
                        firebase.database().ref('com_'+query).push({//change here
                            email: snapshot.val().name,
                            data: post_txt.value,
                            u_id: user.uid,
                            uptime: date_posted
                        }).catch(e => {
                            console.log(e);
                            window.location.href="index.html";
                        });
                        post_txt.value="";
                    }
                });
                show_list(user);
            });
        }
        else{
            var input_element="<img class='mb-4' src='img/pic2-01.png' alt=''id='nav-pic'><input type='email' id='inputEmail' class='form-control' placeholder='Email address' required autofocus ><input type='password' id='inputPassword' class='form-control' placeholder='Password' required><br>";
            var button_element="<button class='btn btn-lg btn-primary btn-block' id='btnLogin'>Login</button>";
            var button_google="<button id='btngoogle' class='btn btn-lg btn-info btn-block'>Google Login</button>";
            var button_signup="<button class='btn btn-lg btn-secondary btn-block' id='btnSignUp'>New account</button>";
            side_menu.innerHTML=div_before_element+input_element+button_element+button_google+button_signup+button_back_home+div_after_element;
            show_list(user);
            login();
            
            //
        }
    });
}
function show_list(user){
    var str_before_time = "<div class='my-3 p-3 rounded box-shadow' style='background-color: rgba(245, 245, 245, 0.5);'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_before_username = "</h6><div class='media text-muted pt-3' ><a href='";
    var str_a = "'><img src='img/ssa.gif' class='";
    var str_after_content = "' alt='' class='mr-2 rounded' style='height:32px; width:32px;'></a><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    
    var postsRef = firebase.database().ref('com_'+query);//change here
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childSnapshot) {
                var childData = childSnapshot.val();
                console.log(childSnapshot.key);
                var user_id=childData.u_id;
                var first_after="<br><button class='btn btn-lg btn-primary' onclick='reply(&apos;"+childSnapshot.key+"&apos;)' id='btnreply'>Go to postpage</button></div>\n";
                if(user){total_post[total_post.length] = str_before_time+childData.uptime+str_before_username +"profile.html?"+user_id+ str_a +user_id+str_after_content+"<strong>"+ childData.email + "</strong><br>" + "<big>"+childData.data+"</big></div>" + first_after;}
                else{total_post[total_post.length] = str_before_time+childData.uptime+str_before_username +"profile.html?"+user_id+ str_a+user_id+str_after_content+"<strong>"+ childData.email + "</strong><br><big>"+ childData.data+"</big></div></div>\n";}
                /*childSnapshot.forEach(function(replySnapshot){
                    if(replySnapshot.key!="email"&&replySnapshot.key!="data"&&replySnapshot.key!="time"){
                    var replyData = replySnapshot.val();
                    //console.log(replySnapshot.key);
                        total_post[total_post.length] = "<br>"+replyData.data+"<br>";//modify this
                    }
                });*/
                storageRef.child("/test/"+user_id).getDownloadURL().then(function (url) {
                    // `url` is the download URL for fileRefDL.value
                        $("."+user_id).each(function() {
                            this.src = url; 
                        });
                }).catch(function (error) {
                    console.log(error.message);
                    storageRef.child("/test/default").getDownloadURL().then(function (url) {
                        $("."+user_id).each(function() {
                            this.src = url; 
                        });
                    });
                });
                first_count+=1;
            });
            document.getElementById('post_list').innerHTML=(total_post.join(''));
            postsRef.on('child_added',function(data){
                second_count+=1;
                if(second_count>first_count){
                    var childData = data.val();
                    var second_after="<br><button class='btn btn-lg btn-primary' onclick='reply(&apos;"+data.key+"&apos;)' id='btnreply'>Go to postpage</button></div>\n";
                    var user_id =childData.u_id;
                    if(user){$('#post_list').append(str_before_time+childData.uptime+str_before_username+"profile.html?"+user_id+ str_a +user_id+ str_after_content+"<strong>"+ childData.email + "</strong><br>" + "<big>"+childData.data+"</big></div>"  + second_after);}
                    else{$('#post_list').append(str_before_time+childData.uptime+str_before_username+"profile.html?"+user_id+ str_a +user_id+ str_after_content+"<strong>"+ childData.email + "</strong><br>" + "<big>"+childData.data+"</big>" +"</div>\n");}
                //window.location="postlist.html?"+query;//change here
                storageRef.child("/test/"+user_id).getDownloadURL().then(function (url) {
                    // `url` is the download URL for fileRefDL.value
                        $("."+user_id).each(function() {
                            this.src = url; 
                        });
                }).catch(function (error) {
                    console.log(error.message);
                    storageRef.child("/test/default").getDownloadURL().then(function (url) {
                        $("."+user_id).each(function() {
                            this.src = url; 
                        });
                    });
                });
                }
            })
        })
        .catch(e => console.log(e.message));
       
    
}

function reply(key_in){
    var packed=query+','+key_in;
    window.location="postpage.html?"+packed;//change here
}
function login(){
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    btnLogin.addEventListener('click', e => {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(function (){window.location.href="postlist.html?"+query;}).catch(e => alert(e.message));
        });
    btnGoogle.addEventListener('click', e => {
        var provider = new firebase.auth.GoogleAuthProvider();
        //alert('signInWithPopup');
        firebase.auth().signInWithPopup(provider).then(function (){window.location.href="postlist.html?"+query;}).catch(function (error) {
            alert('error: ' + error.message);
        })});
    btnSignUp.addEventListener('click', e => {        
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function (){window.location.href="postlist.html?"+query;}).catch(e => alert(e.message));
        });
    }
function back_to_home(){
    window.location.href="index.html";
}
function jump_to_profile(){
    firebase.auth().onAuthStateChanged(function (user) {
    window.location.href="profile.html?"+user.uid;
    });
}
window.onload = function () {
    init();
};
