var user_email = '';
var query;
var user_id = '';
var storageRef=firebase.storage().ref();
var list_num;
var key;
function init(){
    query = window.location.search;
    // Skip the leading ?, which should always be there, 
    // but be careful anyway
    if (query.substring(0, 1) == '?') {
        query = query.substring(1);
    }
    var data = query.split(','); 
    for (i = 0; (i < data.length); i++) {
      data[i] = unescape(data[i]);
    }
    //alert(data[0]+" "+data[1]);
    list_num = data[0];
    key = data[1];
    var user_name = '';
    var side_menu = document.getElementById('side');
    var div_before_element="<div class='info-input'>";
    var button_back_home="<button class='btn btn-lg btn-primary btn-block' onclick='back_to_home()' style='background-color: '>Home</button>"
    var div_after_element="</div>";
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            var Ref = firebase.database().ref("userdata/"+user.uid);
            Ref.once('value').then(function(snapshot) {
                var temp = snapshot.val();
                //alert(temp);
                if(!temp){
                    Ref.update({
                        name:user.email,
                        gender:"unknown",
                        birth: "2000-01-01",
                        hobby: "unknown",
                        introduce: "Hello"
                    })
                }
            });
            user_email = user.email;
            user_name = user.name;
            user_id = user.uid;
            var string_before_email="<img class='mb-4' src='img/ssa.gif' alt=''id='nav-pic'><span style='font-size: 1.5vmin;'>";
            var string_after_email="</span><br><br><button class='btn btn-lg btn-primary btn-block' onclick='jump_to_profile()' id='btnprofile'>Profile</button><button class='btn btn-lg btn-primary btn-block' id='logout-btn'>Logout</button>";
            Ref.once('value').then(function(snapshot) {
                var temp = snapshot.val();
                side_menu.innerHTML=div_before_element+string_before_email+snapshot.val().name+string_after_email+button_back_home+div_after_element;
                var btnLogout = document.getElementById('logout-btn');
                var user_id=user.uid;
                    storageRef.child("/test/"+user_id).getDownloadURL().then(function (url) {
                        // `url` is the download URL for fileRefDL.value
                        document.getElementById('nav-pic').src=url;
                    }).catch(function (error) {
                        console.log(error.message);
                        storageRef.child("/test/default").getDownloadURL().then(function (url) {
                            document.getElementById('nav-pic').src=url;
                        });
                    });
                btnLogout.addEventListener('click', e => {
                    firebase.auth().signOut().then(function(){window.location='index.html'}).catch(function(error){alert(error.message)});
            });
            });
            
        }
        else{
            alert('You have not logged in');
            window.location='index.html';
        }
        show_post(user,list_num,key);
    });
}
function show_post(user,list_num,key){
    var content = document.getElementById('post');
    var refe='com_'+list_num+"/"+key;
    var Post = firebase.database().ref(refe);
    var post_txt = document.getElementById('comment');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    var str_head_time = "<div class='my-3 p-3 rounded box-shadow' style='background-color: rgba(245, 245, 245, 0.5);'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_head = "</h6><div class='media text-muted pt-3' ><a href='profile.html?";
    var str_before_username = "'><img src='img/ssa.gif' class='";
    var str_img = "' alt='' class='mr-2 rounded' style='height:32px; width:32px;'></a><p class='media-body pb-3 mb-0 big lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>";
    var str_head_time_01 = "<div class='my-3 p-3 rounded box-shadow' style='background-color: rgba(245, 245, 245, 1);'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_head_01 = "</h6><div class='media text-muted pt-3' ><a href='profile.html?";
    var str_before_username_01 = "'><img src='img/ssa.gif' class='";
    var str_img_01 = "' alt='' class='mr-2 rounded' style='height:32px; width:32px;'></a><p class='media-body pb-3 mb-0 big lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    //alert('com_'+list_num+"/"+key);
    Post.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childSnapshot) {
                var childData = childSnapshot.val();
                if(childSnapshot.key!="email"&&childSnapshot.key!="data"&&childSnapshot.key!="u_id"&&childSnapshot.key!="uptime"){
                    console.log(childSnapshot.key);
                    var user_id=childData.u_id;
                    console.log("uid "+user_id)
                    total_post[total_post.length] = str_head_time+childData.uptime+str_head+user_id+str_before_username + user_id + str_img + childData.email + "</strong>" + childData.data + str_after_content;
                    storageRef.child("/test/"+user_id).getDownloadURL().then(function (url) {
                        // `url` is the download URL for fileRefDL.value
                            $("."+user_id).each(function() {
                                this.src = url; 
                            });
                    }).catch(function (error) {
                        console.log(error.message);
                        storageRef.child("/test/default").getDownloadURL().then(function (url) {
                            $("."+user_id).each(function() {
                                this.src = url; 
                            });
                        });
                    });
                    
                }
            });
            //alert(snapshot.val().u_id);
            if(snapshot.val()==undefined){window.location.href="index.html";}
            content.innerHTML=str_head_time_01+snapshot.val().uptime+str_head_01+snapshot.val().u_id+str_before_username_01 +snapshot.val().u_id+ str_img_01 + snapshot.val().email+"</strong>"+snapshot.val().data+str_after_content+total_post.join('');
            storageRef.child("/test/"+snapshot.val().u_id).getDownloadURL().then(function (url) {
                // `url` is the download URL for fileRefDL.value
                    $("."+snapshot.val().u_id).each(function() {
                        this.src = url; 
                    });
            }).catch(function (error) {
                console.log(error.message);
                storageRef.child("/test/default").getDownloadURL().then(function (url) {
                    $("."+snapshot.val().u_id).each(function() {
                        this.src = url; 
                    });
                });
            });
            first_count+=1;
        
        })
        .catch(e => console.log(e.message));
        Post.on('child_added',function(data){
            var str_before_username_time="<div class='my-3 p-3 rounded box-shadow' style='background-color: rgba(245, 245, 245, 0.5);'><h6 class='border-bottom border-gray pb-2 mb-0'>"
            var str_before_username = "</h6><div class='media text-muted pt-3' ><a href='"
            var str_a="'><img src='img/ssa.gif' class='";
            var str_img = "' alt='' class='mr-2 rounded' style='height:32px; width:32px;'></a><p class='media-body pb-3 mb-0 big lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
            var str_after_content = "</p></div></div>";
            second_count+=1;
            if(second_count>first_count){
                var childData = data.val();
                content.innerHTML+=str_before_username_time+childData.uptime+str_before_username+"profile.html?"+childData.u_id+str_a+ user_id + str_img + childData.email + "</strong>" + childData.data + str_after_content;
                firebase.auth().onAuthStateChanged(function (user) {
                    var user_id=user.uid;
                    storageRef.child("/test/"+user_id).getDownloadURL().then(function (url) {
                        // `url` is the download URL for fileRefDL.value
                            $("."+user_id).each(function() {
                                this.src = url; 
                            });
                    }).catch(function (error) {
                        console.log(error.message);
                        storageRef.child("/test/default").getDownloadURL().then(function (url) {
                            $("."+user_id).each(function() {
                                this.src = url; 
                            });
                        });
                    });
                });
            }
        });
}
function reply(){
    firebase.auth().onAuthStateChanged(function (user) {
    refer='com_'+list_num+"/"+key;
    user_id=user.uid;
    var postsRef = firebase.database().ref(refer);
    var post_txt = document.getElementById('comment');
    //postsRef
    console.log(refer);
    console.log(post_txt.value);
    var time =new Date();
    var hour = (time.getUTCHours()+8)%24;
    var te_hour= hour>10?hour:"0"+hour;
    var min = time.getUTCMinutes()>9?time.getUTCMinutes():"0"+time.getUTCMinutes();
    var sec = time.getUTCSeconds()>9?time.getUTCSeconds():"0"+time.getUTCSeconds();
    var date_posted= time.getUTCFullYear()+"/"+(time.getUTCMonth()+1)+"/"+(time.getUTCDate()+1)+" "+te_hour+":"+min+":"+sec;
    var Ref = firebase.database().ref("userdata/"+user.uid);
    Ref.once('value').then(function(snapshot) {
        if (post_txt.value != "") {
            firebase.database().ref(refer).push({//change here
                email: snapshot.val().name,
                data: post_txt.value,
                u_id:user_id,
                uptime:date_posted
            }).catch(e => console.log(e.message));
            post_txt.value="";
        }
    });
    
    });
}
function login(){
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    btnLogin.addEventListener('click', e => {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).catch(e => alert(e.message));
        });
    btnGoogle.addEventListener('click', e => {
        var provider = new firebase.auth.GoogleAuthProvider();
        //alert('signInWithPopup');
        firebase.auth().signInWithPopup(provider).catch(function (error) {
            alert('error: ' + error.message);
        })});
    btnSignUp.addEventListener('click', e => {        
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email, password).catch(e => alert(e.message));
        });
    }
function back_to_home(){
    window.location.href="index.html";
}
function jump_to_profile(){
    firebase.auth().onAuthStateChanged(function (user) {
        window.location.href="profile.html?"+user.uid;
    });
}
window.onload = function () {
    init();
};
