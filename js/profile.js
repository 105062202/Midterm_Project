var selectedFile;
var storageRef = firebase.storage().ref();
var query;
function init(){
    query = window.location.search;
    // Skip the leading ?, which should always be there, 
    // but be careful anyway
    if (query.substring(0, 1) == '?') {
        query = query.substring(1);
    }
    if(query.length<1){
        jump_to_home();
    }
    //alert(query);
    var user_email = '';
    var user_name = '';
    var side_menu = document.getElementById('side');
    var div_before_element="<div class='info-input'>";
    var div_after_element="</div>";
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            //alert('in');
            var Ref = firebase.database().ref("userdata/"+user.uid);
            Ref.once('value').then(function(snapshot) {
                var temp = snapshot.val();
                //alert(temp);
                if(!temp){
                    Ref.update({
                        name:user.email,
                        gender:"unknown",
                        birth: "2000-01-01",
                        hobby: "unknown",
                        introduce: "Hello"
                    })
                }
            });
            user_email = user.email;
            user_name = user.name;
            var string_before_email="<img class='mb-4' src='img/ssa.gif' alt=''id='nav-pic'><span style='font-size: 1.5vmin;'>";
            var string_after_email="</span><br><br><button class='btn btn-lg btn-primary btn-block' id='logout-btn'>Logout</button><button class='btn btn-lg btn-primary btn-block' onclick='jump_to_home()'>Home</button>";
            var Ref = firebase.database().ref("userdata/"+user.uid);
            Ref.once('value').then(function(snapshot) {
                side_menu.innerHTML=div_before_element+string_before_email+snapshot.val().name+string_after_email+div_after_element;

                var btnLogout = document.getElementById('logout-btn');
                btnLogout.addEventListener('click', e => {
                    firebase.auth().signOut().then(function(){
                        jump_to_home();
                    }).catch(function(error){alert(error.message)});
                });
                
                var user_id=user.uid;
                storageRef.child("/test/"+user_id).getDownloadURL().then(function (url) {
                    // `url` is the download URL for fileRefDL.value
                    document.getElementById('nav-pic').src=url;
                }).catch(function (error) {
                    console.log(error.message);
                    storageRef.child("/test/default").getDownloadURL().then(function (url) {
                        document.getElementById('nav-pic').src=url;
                    });
                });
                //alert('on');
                show_profile(user,query);
            });
            
        }
        else{
            //alert('Please login first');
            alert('You have not logged in');
            jump_to_home();
        }
    });
}

function show_profile(user,page_uid){
    //alert('in');
    var Ref = firebase.database().ref("userdata/"+page_uid);
    //alert(Ref);
    var before="";
    var between="<div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 big lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var after="</strong></p></div></div>";
    var total_post = [];
    
    var result = "";
    var name_data;
    var gender_data;
    var birth_data ;
    var hobby_data;
    var introduce_data;
    Ref.once('value').then(function(snapshot) {
        //get_data(Ref,snapshot);
        name_data = snapshot.val().name;
        gender_data = snapshot.val().gender;
        birth_data = snapshot.val().birth;
        hobby_data = snapshot.val().hobby;
        introduce_data = snapshot.val().introduce;
        if(user.uid==page_uid){
            //alert("1");
            document.getElementById('content').innerHTML="<img src='img/ssa.gif' width='20%' height='20%' class='pic'>"+after+before+"name"+between+name_data+after+before+"gender"+between+gender_data+after+before+"birthday"+between+birth_data+after+before+"hobby"+between+hobby_data+after+before+"introduce"+between+introduce_data+after+"<div class='media text-muted pt-3' id='edit'></div>"+"<div class='media text-muted pt-3'><button onclick='edit_profile(&apos;"+user.uid+"&apos;,&apos;"+name_data+"&apos;,&apos;"+gender_data+"&apos;,&apos;"+birth_data+"&apos;,&apos;"+hobby_data+"&apos;,&apos;"+introduce_data+"&apos;)' type='button' class='btn btn-lg btn-primary' style='margin:0 auto;'>Edit</button></div>";
            
        }else{
            //alert("2");
            document.getElementById('content').innerHTML=between+"<img src='img/ssa.gif' width='20%' height='20%' class='pic'>"+after+before+"name"+between+name_data+after+before+"gender"+between+gender_data+after+before+"birthday"+between+birth_data+after+before+"hobby"+between+hobby_data+after+before+"introduce"+between+introduce_data+after+"<div class='media text-muted pt-3' id='edit'></div>";
        }
        //result = before+"name"+between+name_data+after+before+"gender"+between+gender_data+after+before+"birthday"+between+birth_data+after+before+"hobby"+between+hobby_data+after+before+"introduce"+between+introduce_data+after;
        var user_id=user.uid;
        storageRef.child("/test/"+page_uid).getDownloadURL().then(function (url) {
            // `url` is the download URL for fileRefDL.value
            $(".pic").each(function() {
                this.src = url; 
            });
        }).catch(function (error) {
            console.log(error.message);
            storageRef.child("/test/default").getDownloadURL().then(function (url) {
                $(".pic").each(function() {
                    this.src = url; 
                });
            });
        });
        }).catch(e => {//alert(e);
    });
    //alert(name+" "+name.key+" "+name_data);
    if(!name_data)name_data=page_uid;
    if(!gender_data)gender_data="unknown";
    if(!birth_data)birth_data="unknown";
    if(!hobby_data)hobby_data="unknown";
    if(!introduce_data)introduce_data="Hello";
    //alert(name_data+" "+gender_data);
    
    }
function edit_profile(uid,name_data,gender_data,birth_data,hobby_data,introduce_data){
    var before="<div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 big lh-125 border-bottom border-gray'><input class='";
    var between="' value='";
    var between_01="' type='date' value='";
    var after="'></p></div></div>";
    var button_save ="<div class='media text-muted pt-3'><button onclick='save(&apos;"+uid+"&apos;)' type='button' class='btn btn-lg btn-primary' style='margin:0 auto;'>Save</button></div>";
    var upload_pic="<span>Avatar</span><br><span>Choise Upload File:<br><input id='selectedFile' type='file' onchange='selectedFile = this.files[0]'></span>";
    document.getElementById('content').innerHTML="<div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 big lh-125 border-bottom border-gray'>"+"<img src='img/ssa.gif' width='20%' height='20%' class='pic'><br>"+upload_pic+"</p></div></div>"+"name"+before+"nameVal"+between+name_data+after+"gender"+before+"genderVal"+between+gender_data+after+"birthday"+before+"birthVal"+between_01+birth_data+after+"hobby"+before+"hobbyVal"+between+hobby_data+after+"introduce"+before+"introduceVal"+between+introduce_data+after+"<div class='media text-muted pt-3' id='edit'></div>"+button_save;
    //alert (document.getElementById('nameVal').val+" second");
    storageRef.child("/test/"+uid).getDownloadURL().then(function (url) {
        // `url` is the download URL for fileRefDL.value
        $(".pic").each(function() {
            this.src = url; 
        });
    }).catch(function (error) {
        console.log(error.message);
        storageRef.child("/test/default").getDownloadURL().then(function (url) {
            $(".pic").each(function() {
                this.src = url; 
            });
        });
    });
}
function save(uid){
    //alert('fuck');
    if(selectedFile){
        var user_id=uid;
            //alert('TEST');
            var fileRef = storageRef.child("/test/"+user_id);
            fileRef.put(selectedFile).then(function (snapshot) {
                console.log('Uploaded a blob or file!');
            }).catch(function (error) {
                console.log(error.message);
            });
    }
    var _name="";
    $('.nameVal').each(function () {
        _name =  ($(this).val());
    });
    var _gender = "";
    $('.genderVal').each(function () {
        _gender = ($(this).val());
    });
    var _birth = "";
    $('.birthVal').each(function () {
        _birth = ($(this).val());
    });
    var _hobby = "";
    $('.hobbyVal').each(function () {
        _hobby = ($(this).val());
    });
    var _introduce = "";
    $('.introduceVal').each(function () {
        _introduce = ($(this).val());
    });
    if(_name==''||_gender==''||_birth==''||_hobby==''||_introduce==''){
        alert("please fill all slots");
    }else{
        /*alert(_name);
        alert('uploading')*/
        var Ref = firebase.database().ref('/userdata/'+uid);
        Ref.update({
            birth: _birth,
            name:_name,
            gender: _gender,
            hobby: _hobby,
            introduce: _introduce
        }).then(function(){
            alert("profile updated")
            window.location.href="profile.html?"+uid;
        });
    }
    
}
/*
    name_data = snapshot.val().name;
    if(!name_data){Ref.update({//change here
        name: user.uid,
    }).then(function(){console.log("success up name");}).catch(e => console.log(e.message));
    console.log('no name');}
    gender_data = snapshot.val().gender;
    if(!gender_data){Ref.update({//change here
        gender: "unknown",
    }).then(function(){console.log("success up gender");}).catch(e => console.log(e.message));
    console.log('no gender');}
    birth_data = snapshot.val().birth;
    if(!birth_data){Ref.update({//change here
        birth: "unknown",
    }).then(function(){console.log("success up birth");}).catch(e => console.log(e.message));
    console.log('no birth');}
    hobby_data = snapshot.val().hobby;
    if(!hobby_data){Ref.update({//change here
        hobby: "unknown",
    }).then(function(){console.log("success up hobby");}).catch(e => console.log(e.message));
    console.log('no hobby');}
    introduce_data = snapshot.val().introduce;
    if(!introduce_data){Ref.update({//change here
        introduce: "Hello",
    }).then(function(){console.log("success up introduce");}).catch(e => console.log(e.message));
    console.log('no introduce');}
}*/
function jump_to_home(){
    window.location.href="index.html";
}


window.onload = function () {
    init();
};/*
var btn_uppic=document.getElementById('btn_up');
btn_uppic.addEventListener('click',e=>{
    var user_id=user.uid;
        //alert('TEST');
        var fileRef = storageRef.child("/test/"+user_id);
        fileRef.put(selectedFile).then(function (snapshot) {
            console.log('Uploaded a blob or file!');
        }).catch(function (error) {
            console.log(error.message);
        });
    }
);
btnDL.addEventListener('click', e => {
    var user_id=user.uid;
    storageRef.child("/test/"+user_id).getDownloadURL().then(function (url) {
        // `url` is the download URL for fileRefDL.value
        window.open(url, "_self");
    }).catch(function (error) {
        console.log(error.message);
    });

});*/

                